# GitLab Docs | Hugo Development Site

This project generates the GitLab documentation website. It is undergoing active development as we work to migrate the
Docs website from Nanoc to Hugo.

If you would like to report an issue with docs.gitlab.com, please visit the [current documentation project](https://gitlab.com/gitlab-org/gitlab-docs).

Visit [this Epic](https://gitlab.com/groups/gitlab-org/-/epics/11891) for the status of the Hugo project.

For information on:

- The build and deployment process, see [doc/index.md](doc/index.md).
- Editing GitLab documentation, see [GitLab Documentation guidelines](https://docs.gitlab.com/ee/development/documentation/).

See also:

- [LICENSE](LICENSE): MIT License.
