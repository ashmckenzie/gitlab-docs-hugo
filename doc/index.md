# GitLab Docs site

The GitLab Docs site comprises documentation from the following projects:

- [GitLab](https://gitlab.com/gitlab-org/gitlab)
- [Omnibus GitLab](https://gitlab.com/gitlab-org/omnibus-gitlab)
- [GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner)
- [GitLab Helm chart](https://gitlab.com/gitlab-org/charts/gitlab)
- [GitLab Operator](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator)

This project combines and publishes the documentation from those projects. For information on configuring your
workstation for local development and local preview of the GitLab docs site, see
[Set up local development and preview](setup.md).
