module gitlab.com/gitlab-org/technical-writing-group/gitlab-docs-hugo

go 1.20

require (
	golang.org/x/exp v0.0.0-20231219180239-dc181d75b848
	gopkg.in/yaml.v3 v3.0.1
)
